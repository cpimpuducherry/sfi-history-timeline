var currentBackground = 0;
var backgrounds = [];
backgrounds[0] = "../img/bhagat_singh.svg";
backgrounds[1] = "../img/che_guevara.svg";

function changeBackground() {
    currentBackground++;
    if (currentBackground > 1) currentBackground = 0;
    
    $('header').fadeOut(500, function() {
        $('header').css({
            'background-image': "url('" + backgrounds[currentBackground] + "')"
        });
        $('header').fadeIn(800);
    });
    
    setTimeout(changeBackground, 6000);
}


jQuery(document).ready(function($){

    $(document).on('opened.fndtn.reveal', function(){
	    $("img").unveil();
	});
	
	

    	setTimeout(changeBackground, 6000);
    
	var $timeline_block = $('.cd-timeline-block');
	//hide timeline blocks which are outside the viewport
	$timeline_block.each(function(){
		if($(this).offset().top > $(window).scrollTop()+$(window).height()*0.75) {
			$(this).find('.cd-timeline-img, .cd-timeline-content').addClass('is-hidden');
		}
	});
	
	var previousScroll = 0; //Thisone//
	
	//on scolling, show/animate timeline blocks when enter the viewport
	$(window).on('scroll', function(){
	    var currentScroll = $(this).scrollTop();
	    if (currentScroll > previousScroll){
		    $timeline_block.each(function(){
			    if( $(this).offset().top <= $(window).scrollTop()+$(window).height()*0.75 && $(this).find('.cd-timeline-img').hasClass('is-hidden') ) {
				    $(this).find('.cd-timeline-img, .cd-timeline-content').removeClass('is-hidden').addClass('bounce-in');
			    }
		    });
        }
        else {
            $timeline_block.each(function(){
                if( $(this).offset().top >= $(window).scrollTop()+$(window).height()*0.75 && $(this).find('.cd-timeline-img').hasClass('bounce-in') ) {
			        $(this).find('.cd-timeline-img, .cd-timeline-content').removeClass('bounce-in').addClass('is-hidden');
            
                }
            });    
        }   
        previousScroll = currentScroll;
	});
});
